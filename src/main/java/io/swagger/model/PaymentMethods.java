package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PaymentMethods
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-06-02T14:31:56.560Z[GMT]")


public class PaymentMethods   {
  @JsonProperty("supportedMethods")
  private String supportedMethods = null;

  @JsonProperty("data")
  @Valid
  private List<String> data = null;

  public PaymentMethods supportedMethods(String supportedMethods) {
    this.supportedMethods = supportedMethods;
    return this;
  }

  /**
   * Get supportedMethods
   * @return supportedMethods
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getSupportedMethods() {
    return supportedMethods;
  }

  public void setSupportedMethods(String supportedMethods) {
    this.supportedMethods = supportedMethods;
  }

  public PaymentMethods data(List<String> data) {
    this.data = data;
    return this;
  }

  public PaymentMethods addDataItem(String dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<String>();
    }
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
   **/
  @Schema(description = "")
  
    public List<String> getData() {
    return data;
  }

  public void setData(List<String> data) {
    this.data = data;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentMethods paymentMethods = (PaymentMethods) o;
    return Objects.equals(this.supportedMethods, paymentMethods.supportedMethods) &&
        Objects.equals(this.data, paymentMethods.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(supportedMethods, data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentMethods {\n");
    
    sb.append("    supportedMethods: ").append(toIndentedString(supportedMethods)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
