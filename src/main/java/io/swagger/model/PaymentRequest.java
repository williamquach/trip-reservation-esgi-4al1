package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * PaymentRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-06-02T14:31:56.560Z[GMT]")
public class PaymentRequest {
    @JsonProperty("paymentMethods")
    private PaymentMethods paymentMethods = null;

    @JsonProperty("paymentDetails")
    private PaymentDetails paymentDetails = null;

    @JsonProperty("userId")
    private UserId userId = null;

    @JsonProperty("commandId")
    private CommandId commandId = null;

    public PaymentRequest paymentMethods(PaymentMethods paymentMethods) {
        this.paymentMethods = paymentMethods;
        return this;
    }

    /**
     * Get paymentMethods
     *
     * @return paymentMethods
     **/
    @Schema(required = true, description = "")
    @NotNull

    @Valid
    public PaymentMethods getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(PaymentMethods paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public PaymentRequest paymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
        return this;
    }

    /**
     * Get paymentDetails
     *
     * @return paymentDetails
     **/
    @Schema(required = true, description = "")
    @NotNull
    @Valid
    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    /**
     * Get userId who pays
     *
     * @return userId
     **/
    @Schema(required = true, description = "")
    @NotNull
    @Valid
    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    /**
     * Get command that user pays
     *
     * @return commandId
     **/
    @Schema(required = true, description = "")
    @NotNull
    @Valid
    public CommandId getCommandId() {
        return commandId;
    }

    public void setCommandId(CommandId commandId) {
        this.commandId = commandId;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentRequest paymentRequest = (PaymentRequest) o;
        return Objects.equals(this.paymentMethods, paymentRequest.paymentMethods) &&
                Objects.equals(this.paymentDetails, paymentRequest.paymentDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paymentMethods, paymentDetails);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class PaymentRequest {\n");

        sb.append("    paymentMethods: ").append(toIndentedString(paymentMethods)).append("\n");
        sb.append("    paymentDetails: ").append(toIndentedString(paymentDetails)).append("\n");
        sb.append("    user: ").append(toIndentedString(userId)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
