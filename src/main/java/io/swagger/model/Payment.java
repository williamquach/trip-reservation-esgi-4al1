package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.api.PaymentStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Payment
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-06-02T14:31:56.560Z[GMT]")
public class Payment implements Serializable {
    @JsonProperty("id")
    private PaymentId id = null;

    @JsonProperty("PaymentMethods")
    private PaymentMethods paymentMethods = null;

    @JsonProperty("paymentDetails")
    private PaymentDetails paymentDetails = null;

    @JsonProperty("userId")
    private UserId userId = null;

    @JsonProperty("commandId")
    private CommandId commandId = null;

    @JsonProperty("retry")
    private int retry = 0;

    @JsonProperty("status")
    private String status = "PENDING";

    public Payment(PaymentId id, PaymentMethods paymentMethods, PaymentDetails paymentDetails, UserId userId, CommandId commandId) {
        this.id = id;
        this.paymentMethods = paymentMethods;
        this.paymentDetails = paymentDetails;
        this.userId = userId;
        this.commandId = commandId;
        this.status = "PENDING";
    }

    public Payment id(PaymentId id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @Schema(description = "")

    @Valid
    public PaymentId getId() {
        return id;
    }

    public void setId(PaymentId id) {
        this.id = id;
    }

    public Payment paymentMethods(PaymentMethods paymentMethods) {
        this.paymentMethods = paymentMethods;
        return this;
    }

    /**
     * Get paymentMethods
     *
     * @return paymentMethods
     **/
    @Schema(description = "")

    @Valid
    public PaymentMethods getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(PaymentMethods paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public Payment paymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
        return this;
    }

    /**
     * Get paymentDetails
     *
     * @return paymentDetails
     **/
    @Schema(description = "")

    @Valid
    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public Payment userId(UserId userId) {
        this.userId = userId;
        return this;
    }

    /**
     * Get userId
     *
     * @return userId
     **/
    @Schema(description = "")
    @Valid
    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = Payment.this.userId;
    }

    public Payment commandId(CommandId commandId) {
        this.commandId = commandId;
        return this;
    }

    /**
     * Get userId
     *
     * @return userId
     **/
    @Schema(description = "")
    @Valid
    public CommandId getCommandId() {
        return commandId;
    }

    public void setCommandId(CommandId commandId) {
        this.commandId = Payment.this.commandId;
    }

    public Payment retry(int retry) {
        this.retry = retry;
        return this;
    }

    /**
     * Get retry times
     *
     * @return retry
     **/
    @Schema(required = true, description = "")
    @NotNull
    public int getRetry() {
        return retry;
    }

    public void setRetry(int retry) {
        this.retry = retry;
    }

    public Payment status(String status) {
        this.status = status;
        return this;
    }

    /**
     * Get status
     *
     * @return status
     **/
    @Schema(required = true, description = "")
    @NotNull
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Payment payment = (Payment) o;
        return Objects.equals(this.id, payment.id) &&
                Objects.equals(this.paymentMethods, payment.paymentMethods) &&
                Objects.equals(this.paymentDetails, payment.paymentDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, paymentMethods, paymentDetails);
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentMethods=" + paymentMethods +
                ", paymentDetails=" + paymentDetails +
                ", userId=" + userId +
                ", commandId=" + commandId +
                ", retry=" + retry +
                ", status=" + status +
                '}';
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
