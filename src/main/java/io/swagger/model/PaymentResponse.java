package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * PaymentResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-06-02T14:31:56.560Z[GMT]")


public class PaymentResponse {
    @JsonProperty("requestId")
    private String requestId = null;

    @JsonProperty("methodName")
    private String methodName = null;

    @JsonProperty("details")
    @Valid
    private List<String> details = new ArrayList<String>();

    @JsonProperty("retry")
    private int retry;

    public PaymentResponse requestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    /**
     * Get requestId
     *
     * @return requestId
     **/
    @Schema(required = true, description = "")
    @NotNull

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public PaymentResponse methodName(String methodName) {
        this.methodName = methodName;
        return this;
    }

    /**
     * Get methodName
     *
     * @return methodName
     **/
    @Schema(required = true, description = "")
    @NotNull

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public PaymentResponse details(List<String> details) {
        this.details = details;
        return this;
    }

    public PaymentResponse addDetailsItem(String detailsItem) {
        this.details.add(detailsItem);
        return this;
    }

    /**
     * Get details
     *
     * @return details
     **/
    @Schema(required = true, description = "")
    @NotNull

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    public PaymentResponse retry(int retry) {
        this.retry = retry;
        return this;
    }

    /**
     * Get retry times
     *
     * @return retry
     **/
    @Schema(required = true, description = "")
    @NotNull
    public int getRetry() {
        return retry;
    }

    public void setRetry(int retry) {
        this.retry = retry;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentResponse paymentResponse = (PaymentResponse) o;
        return Objects.equals(this.requestId, paymentResponse.requestId) &&
                Objects.equals(this.methodName, paymentResponse.methodName) &&
                Objects.equals(this.details, paymentResponse.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, methodName, details);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class PaymentResponse {\n");

        sb.append("    requestId: ").append(toIndentedString(requestId)).append("\n");
        sb.append("    methodName: ").append(toIndentedString(methodName)).append("\n");
        sb.append("    details: ").append(toIndentedString(details)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
