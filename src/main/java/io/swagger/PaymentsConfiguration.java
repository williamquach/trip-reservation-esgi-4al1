package io.swagger;

import io.swagger.api.InMemoryRepository;
import io.swagger.api.PaymentRepository;
import io.swagger.api.PaymentService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaymentsConfiguration {
    @Bean
    public PaymentRepository paymentRepository() {
        return new InMemoryRepository();
    }

    @Bean
    public PaymentService paymentService(){
        return new PaymentService(paymentRepository());
    }
}
