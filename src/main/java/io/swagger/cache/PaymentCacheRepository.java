package io.swagger.cache;

import io.swagger.model.CommandId;
import io.swagger.model.Payment;
import io.swagger.model.UserId;

public interface PaymentCacheRepository extends CacheRepository<Payment> {
    Payment retrieve(UserId userId, CommandId commandId);

    Payment store(Payment payment);
}
