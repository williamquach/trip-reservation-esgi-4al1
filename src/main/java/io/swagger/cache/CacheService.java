package io.swagger.cache;

import io.swagger.model.CommandId;
import io.swagger.model.Payment;
import io.swagger.model.UserId;
import org.springframework.beans.factory.annotation.Autowired;

public class CacheService {
    @Autowired
    private final PaymentCacheRepository paymentCacheRepository;

    public CacheService(PaymentCacheRepository cacheRepository) {
        this.paymentCacheRepository = cacheRepository;
    }

    public Payment storePayment(Payment payment) {
        return this.paymentCacheRepository.store(payment);
    }

    public Payment retrievePayment(UserId userId, CommandId commandId) {
        return this.paymentCacheRepository.retrieve(userId, commandId);

    }
}
