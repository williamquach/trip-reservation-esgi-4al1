package io.swagger.cache;

import com.google.gson.Gson;
import io.swagger.api.PaymentsApiController;
import io.swagger.model.CommandId;
import io.swagger.model.Payment;
import io.swagger.model.UserId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.Jedis;

public class RedisPaymentRepository implements PaymentCacheRepository {

    private static final Logger log = LoggerFactory.getLogger(PaymentsApiController.class);
    private final Jedis jedis;

    public RedisPaymentRepository() {
        jedis = new Jedis("redis-server", 6379);
    }

    @Override
    public Payment retrieve(UserId userId, CommandId commandId) {
        try {
            String json = jedis.get("payment-" + userId.getId() + "-" + commandId.getId());
            return new Gson().fromJson(json, Payment.class);
        } catch (Exception e) {
            log.error("Failed to retrive payment with user " + userId.getId() + " and command " + commandId.getId(), e);
            return null;
        }
    }

    @Override
    public Payment store(Payment payment) {
        try {
            String json = new Gson().toJson(payment);
            jedis.set("payment-" + payment.getUserId().getId() + "-" + payment.getCommandId().getId(), json);
            return payment;
        } catch (Exception e) {
            log.error("Failed to store payment with user " + payment.getUserId().getId() + " and command " + payment.getCommandId().getId(), e);
            return null;
        }
    }
}
