package io.swagger.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.cache.CacheService;
import io.swagger.model.Payment;
import io.swagger.model.PaymentId;
import io.swagger.model.PaymentRequest;
import io.swagger.model.PaymentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-06-02T14:31:56.560Z[GMT]")
@RestController
public class PaymentsApiController implements PaymentsApi {

    private static final Logger log = LoggerFactory.getLogger(PaymentsApiController.class);

    private final ObjectMapper objectMapper;

    private final PaymentService paymentService;

    private final CacheService cacheService;


    @Autowired
    public PaymentsApiController(ObjectMapper objectMapper, PaymentService paymentService, CacheService cacheService) {
        this.objectMapper = objectMapper;
        this.paymentService = paymentService;
        this.cacheService = cacheService;
    }

    public ResponseEntity<PaymentResponse> postPayments(@Valid @RequestBody PaymentRequest body) {
        Payment payment = paymentService.CreatePayment(body.getPaymentMethods(), body.getPaymentDetails(), body.getUserId(), body.getCommandId());
        System.out.println("Payment created: " + payment);

        PaymentId paymentId = payment.getId();
        try {
            var retrievedPayment = cacheService.retrievePayment(payment.getUserId(), payment.getCommandId());
            if (retrievedPayment != null) {
                if (retrievedPayment.getRetry() > 3) {
                    var storedPayment = cacheService.storePayment(payment.status(PaymentStatus.RETRY_EXCEEDED));
                    return ResponseEntity.badRequest().body(new PaymentResponse().methodName("Payment failed - Retry"));
                }
                switch (retrievedPayment.getStatus()) {
                    case PaymentStatus.PAID:
                        return ResponseEntity.ok(new PaymentResponse().methodName("Payment success"));
                    case PaymentStatus.FAILED:
                        return ResponseEntity.ok(new PaymentResponse().methodName("Payment failed"));
                    case PaymentStatus.CANCELLED:
                        return ResponseEntity.ok(new PaymentResponse().methodName("Payment cancelled"));
                    case PaymentStatus.RETRY_EXCEEDED:
                        return ResponseEntity.ok(new PaymentResponse().methodName("Payment retry exceeded"));
                    default:
                        break;
                }
            }
            var executedPayment = paymentService.ExecutePayment(paymentId);
            var storedPayment = cacheService.storePayment(payment);
            return ResponseEntity.ok().body(
                    new PaymentResponse()
                            .methodName("Payment succeed with " + executedPayment.getPaymentMethods().getSupportedMethods())
                            .requestId(paymentId.getId())
                            .addDetailsItem(String.format("Payment '%s' for '%s'", executedPayment.getPaymentDetails().getAmount(), executedPayment.getPaymentDetails().getLabel()))
                            .retry(executedPayment.getRetry())
            );
        } catch (NotFoundException e) {
            log.error("Payment not found with id : " + paymentId.getId(), e);
            return ResponseEntity.badRequest().body(
                    new PaymentResponse()
                            .requestId(paymentId.getId())
                            .methodName("Payment not found with id")
                            .addDetailsItem(body.getPaymentDetails().toString())
                            .retry(payment.getRetry())
            );
        } catch (PaymentFailedException e) {
            log.error("Payment failed", e);
            var retrievedPayment = cacheService.retrievePayment(payment.getUserId(), payment.getCommandId());
            Payment paymentRes;
            if (retrievedPayment != null) {
                paymentRes = cacheService.storePayment(retrievedPayment.retry(retrievedPayment.getRetry() + 1));
            }
            else {
                paymentRes = cacheService.storePayment(payment.retry(payment.getRetry() + 1));
            }
            return ResponseEntity.badRequest().body(
                    new PaymentResponse()
                            .requestId(paymentId.getId())
                            .methodName("Payment failed")
                            .addDetailsItem(body.getPaymentDetails().toString())
                            .retry(paymentRes.getRetry())
            );
        }
    }
}
