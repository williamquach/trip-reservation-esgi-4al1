package io.swagger.api;

import io.swagger.model.Payment;

public class PaymentFailedException extends RuntimeException {
    public PaymentFailedException(String message) {
        super(message);
    }

    public static PaymentFailedException withId(Payment payment){
        return new PaymentFailedException(String.format("Payment %s not found", payment.getId()));
    }
}
