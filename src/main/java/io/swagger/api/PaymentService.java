package io.swagger.api;

import io.swagger.model.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Random;

public class PaymentService {
    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository){
        this.paymentRepository = paymentRepository;
    }

    public Payment CreatePayment(PaymentMethods paymentMethods, PaymentDetails paymentDetails, UserId userId, CommandId commandId){
        Payment payment = new Payment(paymentRepository.nextIdentity(), paymentMethods, paymentDetails, userId, commandId);
        this.paymentRepository.save(payment);
        return payment;
    }

    public Payment getPayment(PaymentId paymentId) throws NotFoundException {
        return this.paymentRepository.byId(paymentId);
    }

    public Payment ExecutePayment(PaymentId paymentId) throws NotFoundException {
        Random random = new Random();
        int i = random.nextInt((1) + 1);
        if(i == 0) {
            return paymentRepository.byId(paymentId);
        } else {
            throw new PaymentFailedException("Payment failed");
        }
    }

    /*public boolean CheckPayment(){
    }

    public void AbortPayment(Payment payment){
    }*/
}
