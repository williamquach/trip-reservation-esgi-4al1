package io.swagger.api;

public class PaymentStatus {
    public static final String PENDING = "PENDING";
    public static final String PAID = "PAID";
    public static final String FAILED = "FAILED";
    public static final String CANCELLED = "CANCELLED";
    public static final String RETRY_EXCEEDED = "RETRY_EXCEEDED";
}
