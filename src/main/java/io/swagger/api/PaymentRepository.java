package io.swagger.api;

import io.swagger.model.Payment;
import io.swagger.model.PaymentId;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository {
    PaymentId nextIdentity();

    void save(Payment payment);

    void delete(PaymentId id);

    Payment byId(PaymentId paymentId) throws NotFoundException;

    List<Payment> findAll();
}