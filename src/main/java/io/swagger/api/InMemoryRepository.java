package io.swagger.api;

import io.swagger.model.Payment;
import io.swagger.model.PaymentId;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class InMemoryRepository implements PaymentRepository{
    private final AtomicInteger count = new AtomicInteger(0);

    private final Map<PaymentId, Payment> data = new ConcurrentHashMap<>();

    @Override
    public PaymentId nextIdentity() {
        return new PaymentId(String.valueOf(count.incrementAndGet()));
    }

    @Override
    public void save(Payment payment) {
        data.put(payment.getId(), payment);
    }

    @Override
    public void delete(PaymentId id) {
        data.remove(id);
    }


    @Override
    public Payment byId(PaymentId paymentId) throws NotFoundException {
        final Payment payment = data.get(paymentId);
        if (payment == null) {
            throw new NotFoundException(404, "No Payment Found");
        }
        return payment;
    }

    @Override
    public List<Payment> findAll() {
        return List.copyOf(data.values());
    }
}
